@extends('admin_layout.app')
@section('header')
@include('admin_layout.header')
@endsection
@section('leftbar')
@include('admin_layout.leftbar')
@endsection
@section('rightbar')
@include('admin_layout.rightbar')
@endsection

@section('content') <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="h2 mr-auto">Biodata {{$student->name}}</h1>
    </section>

    <!-- Main content -->
    <section class="invoice">

        <!-- TO DO List -->

        <div class="row">
            <br>
            @if(session()->has('pesan'))
            <div class="alert alert-success">
                {{ session()->get('pesan') }}
            </div>
            @endif
            <br>
            <img class="profile-user-img img-responsive" height="80px" width="80px"
                src="{{url('')}}/{{$student->image}}" alt="User profile picture"><br>
            <br>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">Nama: <a class="pull-right">{{$student->name}} </a></li>
                <li class="list-group-item">NIM: <a class="pull-right">{{$student->nim}} </a></li>

                <li class="list-group-item">Jenis Kelamin:
                    <a class="pull-right">{{$student->gender == 'P' ? 'Perempuan' : 'Laki-laki'}}</a>
                </li>
                <li class="list-group-item">Jurusan: <a class="pull-right">{{$student->departement}}</a> </li>
                <li class="list-group-item">Alamat:
                    <a class="pull-right">{{$student->address == '' ? 'N/A' : $student->address}}</a>
                </li>
                <li class="list-group-item"><a href="{{ route('adminlte.student.edit',['student' => $student->id]) }}"
                        class="btn btn-primary">Edit
                    </a>
                    <a class="pull-right">
                        <form action="{{ route('adminlte.student.destroy',['student'=>$student->id]) }}" method="POST">
                            @method('DELETE')
                            @csrf

                            <button type="submit" class="btn btn-danger ml-3">Hapus</button>

                        </form>
                    </a>
                </li>
            </ul>

        </div>


        <!-- /.box -->

    </section> <!-- /.content -->
</div> @endsection