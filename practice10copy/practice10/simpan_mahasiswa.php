<?php
    include "koneksi.php";
    include "create_message.php";                  
    $target_dir = "upload/";
    $name = $_FILES['customFile']['name'];
    $target_file = $target_dir . basename($_FILES["customFile"]["name"]);
    if($name != "") {
        $error = false;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["customFile"]["tmp_name"]);
            if($check !== false) {
                create_message("File is an image - " . $check["mime"] . ".","danger","warning");
                header("location:index.php");
                $error = false;
            } else {
                create_message("File is not an image.","danger","warning");
                header("location:index.php");
                $error = false;
            }
        }

        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            create_message("Sorry, only JPG, JPEG, PNG & GIF files are allowed.","danger","warning");
            $error = true;
        }
        
        if (file_exists($target_file)) {
            create_message("Halo. foto gantengnya udah ada didatabase nih, ganti yah :) ","danger","warning");
            $error = true;
        }
        
        if ($_FILES["customFile"]["size"] > 500000) {
            create_message("Sorry, your file is too large.","danger","warning");
            $error = true;
        }
        
        } else {
            $error = false;
        }
    
    if ($error == true) {
        } else {
            if (move_uploaded_file($_FILES["customFile"]["tmp_name"], $target_file)) {
                $query = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat, image) VALUES ('".$_POST['nama_lengkap']."', ".$_POST['kelas_id'].", '".$_POST['alamat']."', '".$name."')";
                if ($conn->query($query) === TRUE) {
                    $conn->close();
                    create_message("Simpan Data Berhasil","success","check");
                    header("location:index.php");
                    exit();
                } else {
                    echo "Error: " . $query . "<br>" . $conn->error;
                    create_message("Error: " . $query . "<br>" . $conn->error,"danger","warning");
                    $conn->close();
                    exit();
                }
            } else {
                create_message("Bro, Fotonya diupload dong biar ganteng :)","danger","warning");
                header("location:index.php");
            }
        }
?>