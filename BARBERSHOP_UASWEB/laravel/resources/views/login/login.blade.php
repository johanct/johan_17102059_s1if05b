<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<title>Data Barber</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/BarberHome/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/BarberHome/assets/fonts/font-awesome.min.css">
</head>
<body>

<nav class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark" id="mainNav">
        <div class="container"><a class="navbar-brand" href="#page-top">Login Admin Barber</a><button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler navbar-toggler-center" type="button" data-toogle="collapse" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="nav navbar-nav ml-auto text-uppercase">


                </ul>
            </div>
        </div>
    </nav>

<div class="container mt-3">
<div class="row">
<div class="col-12">
<div class="py-4 d-flex justify-content-end align-items-center">
<h2 class="mr-auto">Login Admin Barber</h2>
</div>
@if(session()->has('pesan'))
<div class="alert alert-success">
{{ session()->get('pesan') }}
</div>
@endif
<form action="{{ route('login.process') }}" method="POST">
@csrf
<div class="form-group">
<label for="nim">Username</label>
<input type="text"
class="form-control @error('username') is-invalid @enderror"
id="username" name="username" value="{{ old('username') }}">
@error('username')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
<div class="form-group">
<label for="password">Password</label>
<input type="password"
class="form-control @error('password') is-invalid @enderror"
id="password" name="password" value="{{ old('password') }}">
@error('password')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
<button type="submit" class="btn btn-primary mb-2">Daftar</button>
</form>
</div>
</div>
</div>
</body>
</html>