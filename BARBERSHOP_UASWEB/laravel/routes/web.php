<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/costumer/create', 'CostumerController@create')->name('costumer.create');
        // //->middleware('login_auth');
Route::post('/costumer', 'CostumerController@store')->name('costumer.store');
        //->middleware('login_auth');
Route::get('/costumer', 'CostumerController@index')->name('costumer.index');
       //->middleware('login_auth');
Route::get('/costumer/{costumer}', 'CostumerController@show') ->name('costumer.show');

Route::get('/costumer/{costumer}/edit', 'CostumerController@edit')->name('costumer.edit');

Route::patch('/costumer/{costumer}', 'CostumerController@update')->name('costumer.update');

Route::delete('/costumer/{costumer}', 'CostumerController@destroy')->name('costumer.destroy');

Route::get('/login', 'AdminController@index')->name('login.index');
Route::get('/logout', 'AdminController@logout')->name('login.logout');
Route::post('/login', 'AdminController@process')->name('login.process');