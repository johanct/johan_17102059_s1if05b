<?php

namespace App\Http\Controllers;
use App\costumer;
use Illuminate\Http\Request;
use File;

class AdminLTEController extends Controller
{

    public function create(){
        return view('adminlte.costumer.create');
    }
    
    public function show($costumer_id)
    {
        $result = costumer::findOrFail($costumer_id);
        return view('adminlte.costumer.show',['costumer' => $result]);
    }
    
    public function store(Request $request)
    {
        $validateData = $request->validate([
        'kupon' => 'required|size:8,unique:costumers',
        'nama' => 'required|min:3|max:50',
        'usia' => 'required|in:P,L',
        'booking' => 'required',
        'alamat' => '',
        'image' => 'required|file|image|max:1000',
        ]);
        
        $booked = new costumer();
        $booked->kupon = $validateData['kupon'];
        $booked->name = $validateData['nama'];
        $booked->gender = $validateData['usia'];
        $booked->departement = $validateData['booking'];
        $booked->address = $validateData['alamat'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->image->move('assets/images',$namaFile);
            $booked->image = $path;
        }
        $booked->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('adminlte.costumer.index');
    }

    public function index()
    {
        $bookeds = costumer::all();
        return view('adminlte.costumer.index',['costumers' => $bookeds]);
    }
    
    public function edit($costumer_id)
    {
        $result = costumer::findOrFail($costumer_id);
        return view('adminlte.costumer.edit',['costumer' => $result]);
    }

    public function update(Request $request, costumer $costumer)
    {
        $validateData = $request->validate([
        'kupon' => 'required|size:8,unique:costumers',
        'nama' => 'required|min:3|max:50',
        'usia' => 'required|in:P,L',
        'booking' => 'required',
        'alamat' => '',
        'image' => 'file|image|max:1000',
        ]);
        
        $costumer->kupon = $validateData['kupon'];
        $costumer->name = $validateData['nama'];
        $costumer->gender = $validateData['usia'];
        $costumer->departement = $validateData['booking'];
        $costumer->address = $validateData['alamat'];
        if($request->hasFile('image'))
        {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            File::delete($costumer->image);
            $path = $request->image->move('assets/images',$namaFile);
            $costumer->image = $path;
        }
        $costumer->save();
        $request->session()->flash('pesan','Perubahan data berhasil');
        return redirect()->route('adminlte.costumer.show',['costumer' => $costumer->id]);
    }

    public function destroy(Request $request, costumer $costumer)
    {
        File::delete($costumer->image);
        $costumer->delete();
        $request->session()->flash('pesan','Hapus data berhasil');
        return redirect()->route('adminlte.costumer.index');
    }
}