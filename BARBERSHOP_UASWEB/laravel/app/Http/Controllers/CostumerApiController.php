<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Costumer;
use File;

class CostumerApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costumer = costumer::all()->toJson(JSON_PRETTY_PRINT);
        return response($costumer, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validateData = Validator::make($request->all(), [
            'kupon' => 'required|size:8,unique:costumer',
            'nama' => 'required|min:3|max:50',
            'usia' => 'required|in:P,L',
            'booking' => 'required',
            'alamat' => '',
            'image' => 'required|file|image|max:40000',
        ]);
        
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $booked = new costumer();
            $booked->kupon = $request->kupon;
            $booked->name = $request->nama;
            $booked->usia = $request->usia;
            $booked->booking = $request->booking;
            $booked->alamat = $request->alamat;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images', $namaFile);
                $booked->image = $path;
            }
            $booked->save();
            return response()->json([
                "message" => "student record created"
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if (costumer::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'kupon' => 'required|size:8,unique:costumers',
                'nama' => 'required|min:3|max:50',
                'usia' => 'required|in:P,L',
                'booking' => 'required',
                'alamat' => '',
                'image' => 'file|image|max:40000',
            ]);
            
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $booked = new costumer();
                $booked->kupon = $request->kupon;
                $booked->name = $request->nama;
                $booked->usia = $request->usia;
                $booked->booking = $request->booking    ;
                $booked->alamat = $request->alamat;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    File::delete($booked->image);
                    $path = $request->image->move('assets/images', $namaFile);
                    $booked->image = $path;
                }
                $mahasiswa->save();
                return response()->json([
                    "message" => "student record updated"
                ], 201);
            }
        } else {
            return response()->json([
                "message" => "Student not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if (costumer::where('id', $id)->exists()) {
            $costumer = costumer::find($id);
            File::delete($costumer->image);
            $costumer->delete();
            return response()->json([
                "message" => "student record deleted"
            ], 201);
        } else {
            return response()->json([
                "message" => "Student not found"
            ], 404);
        }
    }
}
