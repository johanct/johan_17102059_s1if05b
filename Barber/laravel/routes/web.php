<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcomeHome');
});
Route::get('/register', 'AuthController@getRegister')->name('register');
Route::post('/register', 'AuthController@postRegister');
Route::get('/login', 'AuthController@getLogin')->name('login');
Route::post('/login', 'AuthController@postLogin');

Route::get('/Adminlayout/costumer/logout', 'AuthController@logout')->name('logout');

// Route::get('/Adminlayout/index', 'AdminCostumerController@index')->name('Adminlayout.index');
Route::get('/Adminlayout/costumer/create', 'AdminCostumerController@create')->name('Adminlayout.costumer.create');
// Route::get('/Adminlayout/costumer/edit', 'AdminCostumerController@edit')->name('Adminlayout.costumer.edit');
Route::get('/Adminlayout/costumer', 'AdminCostumerController@index')->name('Adminlayout.costumer.index');
Route::get('/Adminlayout/costumer/{costumer}', 'AdminCostumerController@show')->name('Adminlayout.costumer.show');
Route::post('/Adminlayout/costumer', 'AdminCostumerController@store')->name('Adminlayout.costumer.store');
Route::get('/Adminlayout/costumer/{costumer}/edit', 'AdminCostumerController@edit')->name('Adminlayout.costumer.edit');
Route::patch('/Adminlayout/costumer/{costumer}', 'AdminCostumerController@update')->name('Adminlayout.costumer.update');
Route::delete('/Adminlayout/costumer/{costumer}', 'AdminCostumerController@destroy')->name('Adminlayout.costumer.destroy');



Route::get('/costumer/create', 'CostumerController@create')->name('costumer.create');
// //->middleware('login_auth');
Route::post('/costumer', 'CostumerController@store')->name('costumer.store');
//->middleware('login_auth');
Route::get('/costumer', 'CostumerController@index')->name('costumer.index');
//->middleware('login_auth');
Route::get('/costumer/{costumer}', 'CostumerController@show')->name('costumer.show');

Route::get('/costumer/{costumer}/edit', 'CostumerController@edit')->name('costumer.edit');

Route::patch('/costumer/{costumer}', 'CostumerController@update')->name('costumer.update');

Route::delete('/costumer/{costumer}', 'CostumerController@destroy')->name('costumer.destroy');
