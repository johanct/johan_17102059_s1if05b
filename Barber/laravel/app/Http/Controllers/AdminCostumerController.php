<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costumer;
use File;

class AdminCostumerController extends Controller
{
    public function create()
    {
        return view('Adminlayout.costumer.create');
    }
    public function index()
    {
        $bookeds = costumer::all();
        return view('Adminlayout.costumer.index', ['costumers' => $bookeds]);
    }
    public function edit($costumer_id)
    {
        $result = costumer::findOrFail($costumer_id);
        return view('Adminlayout.costumer.edit', ['costumer' => $result]);
    }
    public function show($costumer_id)
    {

        return view('Adminlayout.costumer.show');
    }
    public function destroy(Request $request, costumer $costumer)
    {
        File::delete($costumer->image);
        $costumer->delete();
        $request->session()->flash('pesan', 'Hapus data berhasil');

        return redirect()->route('Adminlayout.costumer.index');
    }
    public function update(Request $request, costumer $costumer)
    {
        $validateData = $request->validate([
            'kupon' => 'required|size:8,unique:costumers',
            'nama' => 'required|min:3|max:50',
            'usia' => 'required|in:P,L',
            'booking' => 'required',
            'alamat' => '',
            'image' => 'file|image|max:100000',
        ]);

        $costumer->kupon = $validateData['kupon'];
        $costumer->name = $validateData['nama'];
        $costumer->gender = $validateData['usia'];
        $costumer->departement = $validateData['booking'];
        $costumer->address = $validateData['alamat'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            File::delete($costumer->image);
            $path = $request->image->move('assets/images', $namaFile);
            $costumer->image = $path;
        }
        $costumer->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');

        return redirect()->route('Adminlayout.costumer.show', ['costumer' => $costumer->id]);
    }
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'kupon' => 'required|size:8,unique:costumers',
            'nama' => 'required|min:3|max:50',
            'usia' => 'required|in:P,L',
            'booking' => 'required',
            'alamat' => '',
            'image' => 'required|file|image|max:100000',
        ]);

        $booked = new costumer();
        $booked->kupon = $validateData['kupon'];
        $booked->name = $validateData['nama'];
        $booked->usia = $validateData['usia'];
        $booked->booking = $validateData['booking'];
        $booked->alamat = $validateData['alamat'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images', $namaFile);
            $booked->image = $path;
        }
        $booked->save();
        $request->session()->flash('pesan', 'Penambahan data berhasil');
        return redirect()->route('Adminlayout.costumer.index');
    }
}
