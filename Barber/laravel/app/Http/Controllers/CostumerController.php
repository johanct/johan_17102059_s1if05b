<?php

namespace App\Http\Controllers;

use App\Costumer;
use File;
use Illuminate\Http\Request;

class CostumerController extends Controller
{
    public function create()
    {
        return view('costumer.create');
    }


    public function store(Request $request)
    {
        $validateData = $request->validate([
            'kupon' => 'required|size:8,unique:costumers',
            'nama' => 'required|min:3|max:50',
            'usia' => 'required|in:P,L',
            'booking' => 'required',
            'alamat' => '',
            'image' => 'required|file|image|max:100000',
        ]);

        $booked = new costumer();
        $booked->kupon = $validateData['kupon'];
        $booked->name = $validateData['nama'];
        $booked->usia = $validateData['usia'];
        $booked->booking = $validateData['booking'];
        $booked->alamat = $validateData['alamat'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images', $namaFile);
            $booked->image = $path;
        }
        $booked->save();
        $request->session()->flash('pesan', 'Penambahan data berhasil');
        return redirect()->route('costumer.index');
    }

    public function index()
    {
        $booked = costumer::all();
        return view('costumer.index', ['costumers' => $booked]);
    }

    public function show($costumer_id)
    {
        $result = costumer::findOrFail($costumer_id);
        return view('costumer.show', ['costumer' => $result]);
    }

    public function edit($costumer_id)
    {
        $result = costumer::findOrFail($costumer_id);
        return view('costumer.edit', ['costumer' => $result]);
    }

    public function update(Request $request, costumer $costumer)
    {
        $validateData = $request->validate([
            'kupon' => 'required|size:8,unique:costumers',
            'nama' => 'required|min:3|max:50',
            'usia' => 'required|in:P,L',
            'booking' => 'required',
            'alamat' => '',
            'image' => 'file|image|max:1000000',
        ]);
        $costumer->kupon = $validateData['kupon'];
        $costumer->name = $validateData['nama'];
        $costumer->usia = $validateData['usia'];
        $costumer->booking = $validateData['booking'];
        $costumer->alamat = $validateData['alamat'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            File::delete($costumer->image);
            $path = $request->image->move('assets/images', $namaFile);
            $costumer->image = $path;
        }
        $costumer->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');
        return redirect()->route('costumer.show', ['costumer' => $costumer->id]);
    }

    public function destroy(Request $request, costumer $costumer)
    {
        File::delete($costumer->image);
        $costumer->delete();
        $request->session()->flash('pesan', 'Hapus data berhasil');
        return redirect()->route('costumer.index');
    }
}
