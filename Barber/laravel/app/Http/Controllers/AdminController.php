<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Beranda";
        return view('Admin.app', ['data' => $data]);
    }
}
