    <nav class="navbar navbar-dark bg-primary align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="color: rgb(16,18,54);background-color: rgb(81,78,223);padding: 0px;background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/Barbersmash.jpg&quot;);background-size: cover;background-position: center;background-repeat: repeat-y;filter: grayscale(0%) saturate(108%);height: 1615px;">
        <div class="container-fluid d-flex flex-column p-0">
            <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#" style="width: 193px;">
                <div class="sidebar-brand-icon rotate-n-15"></div>
                <div class="sidebar-brand-text mx-3"></div><img class="d-flex align-items-start" src="{{ url('') }}/laravel/vendor/Admin/assets/img/barber.png" style="font-size: 13px;width: 60px;padding: 0px;margin: -38px;height: 60px;"><span class="shadow-lg" style="margin: 42px;">Barber</span>
            </a>
            <hr class="sidebar-divider my-0">
            <ul class="nav navbar-nav text-light" id="accordionSidebar">
                <li class="nav-item" role="presentation"><a class="nav-link active" href="{{ route('Adminlayout.costumer.index') }}" style="font-size: 18px;margin: 0px;padding: 16px;"><i class="fas fa-home" style="font-size: 20px;"></i><span style="color: rgb(255,255,255);font-family: Andada, serif;font-size: 15px;">Dashboard</span></a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="{{ route('Adminlayout.costumer.create') }}" style="font-size: 2px;margin: 0px;padding: 18px;"><i class="fas fa-crutch" style="color: #ffffff;height: 4px;"></i><span style="font-size: 15px;font-family: Andada, serif;">Pendaftaran</span></a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="{{ route('Adminlayout.costumer.index') }}" style="color: #ffffff;font-size: 0px;padding: 11px;margin: 6px;"><i class="fas fa-table" style="color: #ffffff;font-size: 20px;"></i><span style="font-size: 15px;font-family: Andada, serif;">Table</span></a></li>
                <li class="nav-item" role="presentation"></li>
                <li class="nav-item" role="presentation"></li>
            </ul>
            <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
        </div>
    </nav>