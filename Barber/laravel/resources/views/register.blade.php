<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Register - ASEM</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abhaya+Libre">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Andada">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/fonts/fontawesome-all.min.css">
</head>

<body class="bg-gradient-primary" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/barberlok.jpg&quot;);">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="text-monospace text-nowrap text-truncate text-break text-lowercase d-table-row flex-grow-1 bg-register-image" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/Barbershop-Cover.png&quot;);background-position: center;background-size: cover;"></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4" style="font-family: 'Abhaya Libre', serif;font-size: 36px;">Creat New Acount</h4>
                            </div>
                            <form class="user" method="POST" action="{{ route('register')}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <input class="form-control form-control-user {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" id="name" placeholder="First Name" name="name" value="{{ old('name')}}">
                                    @if ($errors->has('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif
                                    <!-- <div class="col-sm-6"><input class="form-control form-control-user" type="text" id="FirstName" placeholder="Last Name" name="last_name"></div> -->
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-control-user {{$errors->has('email') ? 'is-invalid' : ''}}" type="email" id="inputEmail" aria-describedby="emailHelp" placeholder="Email Address" name="email" value="{{ old('email')}}">
                                    @if ($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('email')}}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-control-user {{$errors->has('password') ? 'is-invalid' : ''}}" type="password" id="examplePasswordInput" placeholder="Password" name="password">
                                    @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('password')}}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-control-user {{$errors->has('password_confirmation') ? 'is-invalid' : ''}}" type="password" id="exampleRepeatPasswordInput" placeholder="Repeat Password" name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('password_confirmation')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                </div><button class="btn btn-primary btn-block text-white btn-user" type="submit">Register Account</button>
                                <hr><a class="btn btn-primary btn-block text-white btn-google btn-user" role="button"><i class="fab fa-google"></i>&nbsp; Register with Google</a><a class="btn btn-primary btn-block text-white btn-facebook btn-user" role="button"><i class="fab fa-facebook-f"></i>&nbsp; Register with Facebook</a>
                                <hr>
                            </form>
                            <div class="text-center"><a class="small" href="forgot-password.html">Forgot Password?</a></div>
                            <div class="text-center"><a class="small" href="{{ route('login')}}">Already have an account? Login!</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/jquery.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/chart.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/theme.js"></script>
</body>

</html>