<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link href="../../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="styleshe
et">
<title>Biodata {{$costumer->name}}</title>
</head>
<body>
<div class="container mt-3">
<div class="row">
<div class="col-12">
<div class="pt-3 d-flex justify-content-end align-items-center">
<h1 class="h2 mr-auto">Biodata {{$costumer->name}}</h1>
<a href="{{ route('costumer.edit',['costumer' => $costumer->id]) }}"
class="btn btn-primary">Edit
</a>
<form action="{{ route('costumer.destroy',['costumer'=>$costumer->id]) }}"
method="POST">
@method('DELETE')
@csrf

<button type="submit" class="btn btn-danger ml-3">Hapus</button>

</form>
</div>
<hr>
@if(session()->has('pesan'))
<div class="alert alert-success">
{{ session()->get('pesan') }}
</div>
@endif
<ul>
<li>kupon: {{$costumer->kupon}} </li>
<li>Nama: {{$costumer->name}} </li>
<li>usia:
{{$costumer->usia == 'P' ? 'pemuda' : 'lansia'}}
</li>
<li>booking: {{$costumer->booking}} </li>
<li>Alamat:
{{$costumer->alamat == '' ? 'N/A' : $costumer->alamat}}
</li>
</ul>
</div>
</div>
</div>
</body>
</html>