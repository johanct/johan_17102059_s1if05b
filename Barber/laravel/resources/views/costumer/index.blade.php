<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<title>Data Booking</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - Brand</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/BarberHome/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/BarberHome/assets/fonts/font-awesome.min.css">

</head>
<body>
    <nav class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark" id="mainNav">
        <div class="container"><a class="navbar-brand" href="http://localhost/johan_17102059_s1if05b/practice_laravel%20-%20Copy/">Daftar Booking</a><button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler navbar-toggler-right" type="button" data-toogle="collapse" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="nav navbar-nav ml-auto text-uppercase">

                    <li class="nav-item" role="presentation"><a href="{{ route('costumer.create') }}" class="btn btn-primary">Tambah Booking</a>
                </ul>
            </div>
        </div>
    </nav>
    <header>
<div class="container mt-3">
<div class="row">
<div class="col-12">
<div class="py-4 d-flex justify-content-end align-items-center">
<h2 class="mr-auto">Daftar Booking</h2>
<a href="{{ route('costumer.create') }}" class="btn btn-primary">Tambah Booking</a>

<a href="http://localhost/johan_17102059_s1if05b/practice_laravel%20-%20Copy/" class="btn btn-primary">
Home
</a>
</div>
@if(session()->has('pesan'))
<div class="alert alert-success">
{{ session()->get('pesan') }}
</div>
@endif
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>Foto</th>
<th>kupon</th>
<th>Nama</th>
<th>usia</th>
<th>booking</th>
<th>Alamat</th>
</tr>
</thead>
<tbody>
@forelse ($costumers as $booked)
<tr>
<th>{{$loop->iteration}}</th>
<td><img height="30px" src="{{url('')}}/{{$booked->image}}" class="rounded" alt=""></td>
<td><a href="{{ route('costumer.show',['costumer' => $booked->id]) }}">{{$booked->kupon}}</a></td>
<td>{{$booked->name}}</td>
<td>{{$booked->usia == 'P'?'pemuda':'lansia'}}</td>
<td>{{$booked->booking}}</td>
<td>{{$booked->alamat == '' ? 'N/A' : $booked->alamat}}</td>
</tr>
@empty
<td colspan="6" class="text-center">Tidak ada data...</td>
@endforelse
</tbody>
</table>
</div>
</div>
</div>
</body>
</html>