<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link href="../../../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"
>
<title>Edit booking</title>
</head>
<body>
<div class="container pt-4 bg-white">
<div class="row">
<div class="col-md-8 col-xl-6">
<h1>Edit booking</h1>
<hr>
<form action="{{ route('costumer.update',['costumer' => $costumer->id]) }}" method="POST" enctype="multipart/form-data">
@method('PATCH')
@csrf
<div class="form-group">
<label for="kupon">kupon</label>
<input type="text"
class="form-control @error('kupon') is-invalid @enderror"
id="kupon" name="kupon" value="{{ old('kupon') ?? $costumer->kupon }}">
@error('kupon')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
<div class="form-group">
<label for="nama">Nama Lengkap</label>
<input type="text"
class="form-control @error('name') is-invalid @enderror"
id="nama" name="nama" value="{{ old('name') ?? $costumer->name }}">
@error('name')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
<div class="form-group">
<label>usia</label>
<div>
<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="usia"
id="lansia" value="L"
{{ (old('usia') ?? $costumer->usia)
== 'L' ? 'checked': '' }} >
<label class="form-check-label" for="lansia">lansia</label>
</div>
<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="usia"
id="pemuda" value="P"
{{ (old('usia') ?? $costumer->usia)
== 'P' ? 'checked': '' }} >
<label class="form-check-label" for="pemuda">pemuda</label>
</div>
@error('usia')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
</div>
<div class="form-group">
<label for="booking">booking</label>
<select class="form-control" name="booking" id="booking">
<option value="jam 8"
{{ (old('booking') ?? $costumer->booking)==
'jam 8' ? 'selected': '' }} > jam 8
</option>
<option value="jam 9"
{{ (old('booking') ?? $costumer->booking)==
'jam 9' ? 'selected': '' }} > jam 9
</option>
<option value="jam 10"
{{ (old('booking') ?? $costumer->booking)==
'jam 10' ? 'selected': '' }} > jam 10
</option>
<option value="jam 11"
{{ (old('booking') ?? $costumer->booking)==
'jam 11' ? 'selected': '' }} > jam 11
</option>
<option value="jam 14"
{{(old('booking')??$costumer->booking)==
'jam 14'?'selected':''}} > jam 14
</option>
</select>
@error('booking')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
<div class="form-group">
<label for="alamat">Alamat</label>
<textarea class="form-control" id="alamat" rows="3"
name="alamat">{{ old('alamat') ?? $costumer->alamat}}</textarea>
</div>
<div class="form-group">
<label for="image">Gambar Profile</label>
<br><img height="150px" src="{{url('')}}/{{$costumer->image}}" class="rounded" alt="">
<input type="file" class="form-control-file" id="image" name="image">
@error('image')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>
<button type="submit" class="btn btn-primary mb-2">Update</button>
</form>
</div>
</div>
</div>
</body>
</html>