@extends('Admin.app')
@section('header')
<nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
        <form class="form-inline d-none d-sm-inline-block mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
            </div>
        </form>
        <ul class="nav navbar-nav flex-nowrap ml-auto">
            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-search"></i></a>
                <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto navbar-search w-100">
                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                        </div>
                    </form>
                </div>
            </li>
            <div class="d-none d-sm-block topbar-divider"></div>
            <li class="nav-item dropdown no-arrow" role="presentation">
                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><span class="d-none d-lg-inline mr-2 text-gray-600 small">Welcome To Barber</span><img class="border rounded-circle img-profile" src="{{ url('') }}/laravel/vendor/Admin/assets/img/barber.png"></a>
                    <div class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu"><a class="dropdown-item" role="presentation" href="{{ route('Adminlayout.costumer.create') }}"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Create</a><a class="dropdown-item" role="presentation" href="#"></a>

                        <div class="dropdown-divider"></div><a class="dropdown-item" role="presentation" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
@endsection
@section('leftbar')
@include('Admin.leftbar')
@endsection
@section('rightbar')
@include('Admin.rightbar')
@endsection

@section('content')
<div class="container-fluid">
    <h3 class="text-dark mb-4">Pendaftaran</h3>
    <div class="row mb-3">
        <div class="col-lg-4">
            <div class="card mb-3">
                <div class="card-body text-center shadow"><img class="rounded-circle mb-3 mt-4" src="{{ url('') }}/laravel/vendor/Admin/assets/img/barber.png" width="160" height="160">
                    <!-- <form action="{{ route('Adminlayout.costumer.store') }}" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="image">Gambar Profile</label>
                            <input type="file" class="form-control-file" id="image" name="image">
                            @error('image')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div> -->
                </div>
            </div>
            <div class="card shadow mb-4">

            </div>
        </div>
        <div class="col-lg-8">


            <div class="row">
                <div class="col">
                    <div class="card shadow mb-3">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Pendaftaran Costumer</p>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('Adminlayout.costumer.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="kupon">KUPON</label>
                                    <input type="text" class="form-control @error('kupon') is-invalid @enderror" id="kupon" name="kupon" value="{{ old('kupon') }}">
                                    @error('kupon')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}">
                                    @error('nama')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Usia</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="usia" id="lansia" value="L" {{ old('usia')=='L' ? 'checked': ''  }}>
                                            <label class="form-check-label" for="lansia">LanSia</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="usia" id="pemuda" value="P" {{ old('usia')=='P' ? 'checked': '' }}>
                                            <label class="form-check-label" for="pemuda">pemuda</label>
                                        </div>
                                        @error('usia')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="booking">booking</label>
                                    <select class="form-control" name="booking" id="booking">
                                        <option value="Jam 8" {{ old('booking')=='Jam 8' ? 'selected': '' }}>
                                            Jam 8
                                        </option>
                                        <option value="Jam 9" {{ old('booking')=='Jam 9' ? 'selected': '' }}>
                                            Jam 9
                                        </option>
                                        <option value="Jam 10" {{ old('booking')=='Jam 10' ? 'selected': '' }}>
                                            Jam 10
                                        </option>
                                        <option value="Jam 11" {{ old('booking')=='Jam 11' ? 'selected': '' }}>
                                            Jam 11
                                        </option>
                                        <option value="Jam 14" {{ old('booking')=='Jam 14' ? 'selected': '' }}>
                                            Jam 14
                                        </option>
                                    </select>
                                    @error('booking')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" id="alamat" rows="3" name="alamat">{{ old('alamat') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="image">Gambar Profile</label>
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    @error('image')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>



                                <button type="submit" class="btn btn-primary mb-2">Daftar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>

</div>
</div>







@endsection