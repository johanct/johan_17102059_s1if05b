<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>EDIT</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abhaya+Libre">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Andada">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/fonts/fontawesome-all.min.css">
</head>

<body class="bg-gradient-primary" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/barberlok.jpg&quot;);">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="text-monospace text-nowrap text-truncate text-break text-lowercase d-table-row flex-grow-1 bg-register-image" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/Barbershop-Cover.png&quot;);background-position: center;background-size: cover;"></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="col-md-8 col-xl-6">
                                <h1>Pendaftaran Costumer</h1>
                                <hr>

                                <form action="{{ route('costumer.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="kupon">KUPON</label>
                                        <input type="text" class="form-control @error('kupon') is-invalid @enderror" id="kupon" name="kupon" value="{{ old('kupon') }}">
                                        @error('kupon')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}">
                                        @error('nama')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Usia</label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="usia" id="lansia" value="L" {{ old('usia')=='L' ? 'checked': ''  }}>
                                                <label class="form-check-label" for="lansia">LanSia</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="usia" id="pemuda" value="P" {{ old('usia')=='P' ? 'checked': '' }}>
                                                <label class="form-check-label" for="pemuda">pemuda</label>
                                            </div>
                                            @error('usia')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="booking">booking</label>
                                        <select class="form-control" name="booking" id="booking">
                                            <option value="Jam 8" {{ old('booking')=='Jam 8' ? 'selected': '' }}>
                                                Jam 8
                                            </option>
                                            <option value="Jam 9" {{ old('booking')=='Jam 9' ? 'selected': '' }}>
                                                Jam 9
                                            </option>
                                            <option value="Jam 10" {{ old('booking')=='Jam 10' ? 'selected': '' }}>
                                                Jam 10
                                            </option>
                                            <option value="Jam 11" {{ old('booking')=='Jam 11' ? 'selected': '' }}>
                                                Jam 11
                                            </option>
                                            <option value="Jam 14" {{ old('booking')=='Jam 14' ? 'selected': '' }}>
                                                Jam 14
                                            </option>
                                        </select>
                                        @error('booking')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <textarea class="form-control" id="alamat" rows="3" name="alamat">{{ old('alamat') }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="image">Gambar Profile</label>
                                        <input type="file" class="form-control-file" id="image" name="image">
                                        @error('image')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <button type="submit" class="btn btn-primary mb-2">Daftar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>

</html>