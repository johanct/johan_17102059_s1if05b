<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>EDIT</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abhaya+Libre">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Andada">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/fonts/fontawesome-all.min.css">
</head>

<body class="bg-gradient-primary" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/barberlok.jpg&quot;);">
    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="text-monospace text-nowrap text-truncate text-break text-lowercase d-table-row flex-grow-1 bg-register-image" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/Barbershop-Cover.png&quot;);background-position: center;background-size: cover;"></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">

                            <h1>Edit booking</h1>
                            <hr>
                            <form action="{{ route('costumer.update',['costumer' => $costumer->id]) }}" method="POST" enctype="multipart/form-data">
                                @method('PATCH')
                                @csrf
                                <div class="form-group">
                                    <label for="kupon">kupon</label>
                                    <input type="text" class="form-control @error('kupon') is-invalid @enderror" id="kupon" name="kupon" value="{{ old('kupon') ?? $costumer->kupon }}">
                                    @error('kupon')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="nama" name="nama" value="{{ old('name') ?? $costumer->name }}">
                                    @error('name')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>usia</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="usia" id="lansia" value="L" {{ (old('usia') ?? $costumer->usia)== 'L' ? 'checked': '' }}>
                                            <label class="form-check-label" for="lansia">lansia</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="usia" id="pemuda" value="P" {{ (old('usia') ?? $costumer->usia)== 'P' ? 'checked': '' }}>
                                            <label class="form-check-label" for="pemuda">pemuda</label>
                                        </div>
                                        @error('usia')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="booking">booking</label>
                                    <select class="form-control" name="booking" id="booking">
                                        <option value="jam 8" {{ (old('booking') ?? $costumer->booking)=='jam 8' ? 'selected': '' }}> jam 8
                                        </option>
                                        <option value="jam 9" {{ (old('booking') ?? $costumer->booking)=='jam 9' ? 'selected': '' }}> jam 9
                                        </option>
                                        <option value="jam 10" {{ (old('booking') ?? $costumer->booking)=='jam 10' ? 'selected': '' }}> jam 10
                                        </option>
                                        <option value="jam 11" {{ (old('booking') ?? $costumer->booking)=='jam 11' ? 'selected': '' }}> jam 11
                                        </option>
                                        <option value="jam 14" {{(old('booking')??$costumer->booking)=='jam 14'?'selected':''}}> jam 14
                                        </option>
                                    </select>
                                    @error('booking')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" id="alamat" rows="3" name="alamat">{{ old('alamat') ?? $costumer->alamat}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="image">Gambar Profile</label>
                                    <br><img height="150px" src="{{url('')}}/{{$costumer->image}}" class="rounded" alt="">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    @error('image')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>