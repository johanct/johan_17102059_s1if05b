<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Dashboard - Barber</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abhaya+Libre">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Andada">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/fonts/fontawesome-all.min.css">
</head>

<body id="page-top">
    <div class="text-left" id="wrapper">
        @yield('leftbar')
        <!-- leftbar secstion -->


        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                @yield('header')
                <!-- header seaction -->

                <div class="container-fluid">
                    @yield('rightbar')
                    <!-- rightbar seaction -->




                    @yield('content')
                    <!--  -->


                </div>
            </div>
            <div class="col">
                <div class="row"></div>
            </div>
        </div>
    </div>
    </div>
    <footer class="bg-white sticky-footer">
        <div class="container my-auto">
            <div class="text-center my-auto copyright"><span>Copyright © Barber 2020</span></div>
        </div>
    </footer>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/jquery.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/chart.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/theme.js"></script>
</body>

</html>