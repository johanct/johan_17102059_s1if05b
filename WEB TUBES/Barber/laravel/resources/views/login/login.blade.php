<!-- <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - Barber</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abhaya+Libre">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Andada">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/fonts/fontawesome-all.min.css">
</head>

<body class="bg-gradient-primary" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/Barbershop-Cover.png&quot;);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/True-North-Barbershop.jpeg&quot;);"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">Login&nbsp;</h4>
                                    </div>
                                    @if(session()->has('pesan'))
                                    <div class="alert alert-success">
                                        {{ session()->get('pesan') }}
                                    </div>
                                    @endif
                                    <form action="{{ route('login.process') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="nim">Username</label>
                                            <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" value="{{ old('username') }}">
                                            @error('username')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" value="{{ old('password') }}">
                                            @error('password')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <div class="form-check"><input class="form-check-input custom-control-input" type="checkbox" id="formCheck-1"><label class="form-check-label custom-control-label" for="formCheck-1">Remember Me</label></div>
                                            </div>
                                        </div><button class="btn btn-primary btn-block text-white btn-user" type="submit">Login</button>
                                        <hr><a class="btn btn-primary btn-block text-white btn-google btn-user" href="http://localhost/WEB%20TUBES/Barber/" role="button"><i></i>back</a>
                                        <hr>
                                    </form>
                                    <div class="text-center"><a class="small" href="forgot-password.html">Forgot Password?</a></div>
                                    <div class="text-center"><a class="small" href="http://localhost/WEB%20TUBES/Barber/">back</a></div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/jquery.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/chart.min.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="{{ url('') }}/laravel/vendor/Admin/assets/js/theme.js"></script>
</body>

</html> -->