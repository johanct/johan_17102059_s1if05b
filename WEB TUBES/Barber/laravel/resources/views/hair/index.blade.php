<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Data Booking</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - Brand</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/BarberHome/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/BarberHome/assets/fonts/font-awesome.min.css">

</head>

<body>
    <nav class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark" id="mainNav">
        <div class="container"><a class="navbar-brand" href="http://localhost/WEB%20TUBES/Barber/">Hair Style</a><i class="fa fa-bars"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="nav navbar-nav ml-auto text-uppercase">


                </ul>
            </div>
        </div>
    </nav>
    <header>
        <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="container"><a class="navbar-brand"></a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="nav navbar-nav ml-auto text-uppercase">

                        </div>
                        @if(session()->has('pesan'))
                        <div class="alert alert-success">
                            {{ session()->get('pesan') }}
                        </div>
                        @endif
                        <div>
                            <div class="modal-dialog modal-lg text-center" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-8 mx-auto">
                                                    <div class="modal-body">
                                                        <h2 class="text-uppercase">Hair Style</h2>
                                                        @forelse ($hairs as $potong)
                                                        <div>
                                                            <p>No {{$loop->iteration}}<img height="30px" src="{{url('')}}/{{$potong->image}}" class="img-fluid d-block mx-auto" alt=""></p>

                                                            <p>Nama Potongan Rambut: {{$potong->name}}</p>
                                                            <p>Harga Potongan Rambut: {{$potong->harga}}</p>

                                                        </div>
                                                        @empty
                                                        <td colspan="6" class="text-center">Tidak ada data...</td>
                                                        @endforelse



                                                        <!-- </ul><button class="btn btn-primary" data-dismiss="modal" type="button"><i class="fa fa-times"></i><span>&nbsp;Close Project</span></button> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
</body>

</html>