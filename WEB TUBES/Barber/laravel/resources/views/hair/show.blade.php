<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>show</title>
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abel">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abhaya+Libre">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Andada">
    <link rel="stylesheet" href="{{ url('') }}/laravel/vendor/Admin/assets/fonts/fontawesome-all.min.css">
    <title>show {{$hair->name}}</title>
</head>

<body class="bg-gradient-primary" style="background-image: url(&quot;{{ url('') }}/laravel/vendor/Admin/assets/img/Barbershop-Cover.png&quot;);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">

                        <div class="p-5">
                            <div class="pt-3 d-flex justify-content-end align-items-center">
                                <h1 class="h2 mr-auto">Biodata {{$hair->name}}</h1>
                                <a href="{{ route('hair.edit',['hair' => $hair->id]) }}" class="btn btn-primary">Edit
                                </a>
                                <form action="{{ route('hair.destroy',['hair'=>$hair->id]) }}" method="POST">
                                    @method('DELETE')
                                    @csrf

                                    <button type="submit" class="btn btn-danger ml-3">Hapus</button>

                                </form>
                                </a>
                                <form action="{{ route('AdminHair.hair.index') }}">


                                    <button type="submit" class="btn btn-warning ml-3">Back</button>

                                </form>
                            </div>
                            <hr>
                            @if(session()->has('pesan'))
                            <div class="alert alert-success">
                                {{ session()->get('pesan') }}
                            </div>
                            @endif
                            <ul>

                                <li>Nama: {{$hair->name}} </li>
                                <li>Harga: {{$hair->harga}} </li>
                                <div class="form-group">
                                    <label for="image">Style Rambut</label>
                                    <br><img height="150px" src="{{url('')}}/{{$hair->image}}" class="rounded" alt="">

                                    @error('image')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


</body>

</html>