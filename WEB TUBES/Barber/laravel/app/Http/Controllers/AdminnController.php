<?php

namespace App\Http\Controllers;

use App\Adminn;
use Illuminate\Http\Request;

class AdminnController extends Controller
{
    public function index()
    {
        return view('login.login');
    }
    public function process(Request $request)
    {
        $validateData = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        $result = Adminn::where('username', '=', $validateData['username'])->first();
        if ($result) {
            if (($request->password == $result->password)) {
                session(['username' => $request->username]);
                return redirect('/costumer');
            } else {
                return back()->withInput()->with('pesan', "Login Gagal");
            }
        } else {
            return back()->withInput()->with('pesan', "Login Gagal");
        }
    }
    public function logout()
    {
        session()->forget('username');
        return redirect('welcomeHome')->with('pesan', 'Logout berhasil');
    }
}
