<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costumer;
use File;
use Validator;

class CostumerApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costumers = costumer::all()->toJson(JSON_PRETTY_PRINT);
        return response($costumers, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'kupon' => 'required|size:8,unique:costumer,kupon',
            'nama' => 'required|min:3|max:50',
            'usia' => 'required|in:P,L',
            'booking' => 'required',
            'alamat' => '',
            'image' => 'required|file|image|max:1000',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $booked = new costumer();
            $booked->kupon = $request->kupon;
            $booked->name = $request->nama;
            $booked->usia = $request->usia;
            $booked->booking = $request->booking;
            $booked->alamat = $request->alamat;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images', $namaFile);
                $booked->image = $path;
            }
            $booked->save();
            return response()->json([
                "message" => "costumer record created"
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (costumer::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'kupon' => 'required|size:8,unique:costumer,kupon',
                'nama' => 'required|min:3|max:50',
                'usia' => 'required|in:P,L',
                'booking' => 'required',
                'alamat' => '',
                'image' => 'required|file|image|max:1000',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $booked = costumer::find($id);
                $booked->kupon = $request->kupon;
                $booked->name = $request->nama;
                $booked->usia = $request->usia;
                $booked->booking = $request->booking;
                $booked->alamat = $request->alamat;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    File::delete($booked->image);
                    $path = $request->image->move('assets/images', $namaFile);
                    $booked->image = $path;
                }
                $booked->save();
                return response()->json([
                    "message" => "costumer record updated"
                ], 201);
            }
        } else {
            return response()->json([
                "message" => "costumer not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (costumer::where('id', $id)->exists()) {
            $booked = costumer::find($id);
            File::delete($booked->image);
            $booked->delete();
            return response()->json([
                "message" => "costumer record deleted"
            ], 201);
        } else {
            return response()->json([
                "message" => "costumer not found"
            ], 404);
        }
    }
}
