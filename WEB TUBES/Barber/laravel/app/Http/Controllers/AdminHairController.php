<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hair;
use File;

class AdminHairController extends Controller
{
    public function create()
    {
        return view('AdminHair.hair.create');
    }
    public function index()
    {
        $potong = hair::all();
        return view('AdminHair.hair.index', ['hairs' => $potong]);
    }
    public function edit($hair_id)
    {
        $result = hair::findOrFail($hair_id);
        return view('AdminHair.hair.edit', ['hair' => $result]);
    }


    public function show($hair_id)
    {
        $result = hair::findOrFail($hair_id);
        return view('AdminHair.hair.show');
    }
    public function destroy(Request $request, hair $hair)
    {
        File::delete($hair->image);
        $hair->delete();
        $request->session()->flash('pesan', 'Hapus data berhasil');
        return redirect()->route('AdminHair.hair.index');
    }
    public function update(Request $request, hair $hair)
    {
        $validateData = $request->validate([

            'nama' => 'required|min:3|max:50',
            'harga' => 'required|size:8,unique:hairs,harga',
            'image' => 'file|image|max:1000000',
        ]);
        $hair->name = $validateData['nama'];
        $hair->harga = $validateData['harga'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            File::delete($hair->image);
            $path = $request->image->move('assets/images', $namaFile);
            $hair->image = $path;
        }
        $hair->save();
        $request->session()->flash('pesan', 'Perubahan data berhasil');
        return redirect()->route('AdminHair.hair.show', ['hair' => $hair->id]);
    }
    public function store(Request $request)
    {
        $validateData = $request->validate([

            'nama' => 'required|min:3|max:50',
            'harga' => 'required|size:8,unique:hairs,harga',
            'image' => 'required|file|image|max:100000',

        ]);

        $potong = new hair();

        $potong->name = $validateData['nama'];
        $potong->harga = $validateData['harga'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images', $namaFile);
            $potong->image = $path;
        }
        $potong->save();
        $request->session()->flash('pesan', 'Penambahan data berhasil');
        return redirect()->route('AdminHair.hair.index');
    }
}
