-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jul 21, 2020 at 09:37 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barber`
--

-- --------------------------------------------------------

--
-- Table structure for table `costumers`
--

CREATE TABLE `costumers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kupon` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usia` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `image` blob,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `costumers`
--

INSERT INTO `costumers` (`id`, `kupon`, `name`, `usia`, `booking`, `alamat`, `image`, `created_at`, `updated_at`) VALUES
(4, 17102090, 'WIWIT WIAJSENA', 'P', 'Jam 14', 'cilacap', 0x6173736574732f696d616765735c757365722d313539353330353736352e6a7067, '2020-07-20 21:29:25', '2020-07-20 21:29:25'),
(5, 17102072, 'WIWIT WIAJSENA', 'P', 'Jam 14', 'semarang', 0x6173736574732f696d616765735c757365722d313539353330363239372e6a7067, '2020-07-20 21:38:18', '2020-07-20 21:38:18'),
(6, 17102090, 'wiwit', 'P', 'Jam 14', 'purwokerto', 0x6173736574732f696d616765735c757365722d313539353330373034352e6a706567, '2020-07-20 21:50:45', '2020-07-20 21:50:45'),
(7, 17102060, 'fahri', 'P', 'Jam 14', 'tegal', 0x6173736574732f696d616765735c757365722d313539353330373439382e706e67, '2020-07-20 21:58:18', '2020-07-20 21:58:18'),
(8, 17102090, 'WIWIT WIAJSENA', 'P', 'Jam 14', 'cilacap', 0x6173736574732f696d616765735c757365722d313539353330393434322e706e67, '2020-07-20 22:30:42', '2020-07-20 22:30:42'),
(9, 12312349, 'asdasd', 'P', 'Jam 11', 'banjar', 0x6173736574732f696d616765735c757365722d313539353331313631302e6a7067, '2020-07-20 23:06:50', '2020-07-20 23:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggans`
--

CREATE TABLE `pelanggans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kupon` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usia` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `image` blob,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggans`
--

INSERT INTO `pelanggans` (`id`, `kupon`, `nama`, `usia`, `booking`, `address`, `image`, `created_at`, `updated_at`) VALUES
(1, 17102072, 'wiwit farianto', 'L', 'Jam 7', 'pwt', 0x6173736574732f696d616765735c757365722d313539353034383036332e6a7067, '2020-07-17 21:54:23', '2020-07-17 21:54:23'),
(2, 17102072, 'wiwit farianto', 'L', 'Jam 7', 'pwt', 0x6173736574732f696d616765735c757365722d313539353034383238342e6a7067, '2020-07-17 21:58:04', '2020-07-17 21:58:04'),
(3, 17102072, 'agung', 'P', 'Jam 9', 'maos', 0x6173736574732f696d616765735c757365722d313539353034383730332e706e67, '2020-07-17 22:05:03', '2020-07-17 22:05:03'),
(4, 17102090, 'wiwit farianto', 'P', 'Jam 11', 'pwt', 0x6173736574732f696d616765735c757365722d313539353035373031382e6a7067, '2020-07-18 00:23:38', '2020-07-18 00:23:38'),
(5, 12312345, 'bus', 'P', 'Jam 11', 'clp', 0x6173736574732f696d616765735c757365722d313539353035393130342e6a7067, '2020-07-18 00:58:24', '2020-07-18 00:58:24'),
(6, 17102090, 'wiwit farianto', 'L', 'Jam 16', 'clp', 0x6173736574732f696d616765735c757365722d313539353035393233332e6a7067, '2020-07-18 01:00:33', '2020-07-18 01:00:33'),
(7, 12312345, 'wiwit farianto', 'P', 'Jam 17', 'pwt', 0x6173736574732f696d616765735c757365722d313539353039313234312e6a7067, '2020-07-18 09:54:02', '2020-07-18 09:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(14, 'aminaku', 'aminamin12@gmail.com', NULL, '$2y$10$f.jul5TnYQl9hsX8j16vU.xRRz7hhtmPBwkSFu6.kBpOiwIG7LPA.', NULL, '2020-07-17 01:14:39', '2020-07-17 01:14:39'),
(15, 'ahmad', 'bagusbagus12@gmail.com', NULL, '$2y$10$sWoIU1ULUIqyTTcXVD/ya.wV3bZs3HS3ip2qhdsr8sMlu4uqxBkl6', NULL, '2020-07-17 01:50:32', '2020-07-17 01:50:32'),
(16, 'akuahmad', 'akuaku12@gmail.com', NULL, '$2y$10$06p3B/POji1K17xpqQsnUezr0K/oj/a3zORgbUaGEo66B4jiDR42y', NULL, '2020-07-17 01:59:09', '2020-07-17 01:59:09'),
(17, 'wiwit farianto', 'farianto@gamil.com', NULL, '$2y$10$hN7nxL9b00dsv23xBKKpTegZAZBORi4eHfsCXQKKgq67M5oAmnaZe', NULL, '2020-07-17 02:01:05', '2020-07-17 02:01:05'),
(18, 'purwanto', 'kangdarman2@gmail.com', NULL, '$2y$10$3MKgN3oNn6wC4k9CdrX42eM25/EpUuDFnfHuISKSRo6Z9IZv5fR8m', NULL, '2020-07-17 10:00:56', '2020-07-17 10:00:56'),
(19, 'bambang', 'bajingan12@gmail.com', NULL, '$2y$10$kJEx/t0Mcc//MARppPbfzudmSdCti9RfgDG8Y/AWK2GON0wp02Quq', NULL, '2020-07-17 10:06:37', '2020-07-17 10:06:37'),
(20, 'agung', 'agungpurnomo123@gmail.com', NULL, '$2y$10$h.gvVe6mODl9z6TDpYbSf.vdgs48cB1GQ3BBw3SSL1c4VjjixyjNO', NULL, '2020-07-17 20:51:19', '2020-07-17 20:51:19'),
(21, 'agung purnomo', 'sembilan19@gmail.com', NULL, '$2y$10$EhmvYgCdp1bkTxnq3L7FK.vUKLTjQ8lTJ7/s9AX/T/jsbJIiGbZva', NULL, '2020-07-20 19:57:48', '2020-07-20 19:57:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costumers`
--
ALTER TABLE `costumers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelanggans`
--
ALTER TABLE `pelanggans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `costumers`
--
ALTER TABLE `costumers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pelanggans`
--
ALTER TABLE `pelanggans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
