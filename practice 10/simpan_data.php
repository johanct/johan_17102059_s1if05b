<?php
    include "koneksi.php";
    include "create_message.php";
    $target_dir = "upload/";
    $name = $_FILES['customFile']['name'];
    $target_file = $target_dir . basename($_FILES["customFile"]["name"]);

    if($name != "") {
      $error = false;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      if(isset($_POST["submit"])) {
          $check = getimagesize($_FILES["customFile"]["tmp_name"]);
          if($check !== false) {
              create_message("File is an image - " . $check["mime"] . ".","danger","warning");
              header("location:index.php");
              $error = false;
          } else {
              create_message("File is not an image.","danger","warning");
              header("location:index.php");
              $error = false;
          }
      }
          if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
              create_message("Sorry, only JPG, JPEG, PNG & GIF files are allowed.","danger","warning");
              $error = true;
          }
      
      if (file_exists($target_file)) {
          create_message("Sorry, file already exists.","danger","warning");
          $error = true;
      }
      
      if ($_FILES["customFile"]["size"] > 500000) {
          create_message("Sorry, your file is too large.","danger","warning");
          $error = true;
      }
      
      } else {
          $error = false;
      }
  
    
    if ($error == true) {
        } else {
            if (move_uploaded_file($_FILES["customFile"]["tmp_name"], $target_file)) {
              if(isset($_POST['mahasiswa_id'])){
                //Kondisi Update
                $sql = "UPDATE mahasiswa SET nama_lengkap = '".$_POST['nama_lengkap']."', alamat = '".$_POST['alamat']."', image = '".$name."' ,kelas_id = '".$_POST['kelas_id']."' WHERE (`mahasiswa_id` ='".$_POST['mahasiswa_id']."');";
                if ($conn->query($sql) === TRUE) {
                    $conn->close();
                    create_message("Ubah Data Berhasil","success","check");
                    header("location:index.php");
                    exit();
                } else {
                    $conn->close();
                    create_message("Error: " . $sql . "<br>" . $conn->error,"danger","warning");
                    header("location:index.php");
                    exit();
                }
              }else{
                  //Kondisi Insert
                  $sql = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat) VALUES ('".$_POST['nama_lengkap']."', ".$_POST['kelas_id'].", '".$_POST['alamat']."')";
                  if ($conn->query($sql) === TRUE) {
                      $conn->close();
                      create_message("Simpan Data Berhasil","success","check");
                      header("location:index.php");
                      exit();
                  } else {
                      $conn->close();
                      create_message("Error: " . $sql . "<br>" . $conn->error,"danger","warning");
                      header("location:index.php");
                      exit();
                  }
              }
        
            } else {
              if(isset($_POST['mahasiswa_id'])){
                //Kondisi Update
                $sql = "UPDATE mahasiswa SET nama_lengkap = '".$_POST['nama_lengkap']."', alamat = '".$_POST['alamat']."', kelas_id = '".$_POST['kelas_id']."' WHERE (`mahasiswa_id` ='".$_POST['mahasiswa_id']."');";
                if ($conn->query($sql) === TRUE) {
                    $conn->close();
                    create_message("Ubah Data Berhasil","success","check");
                    header("location:index.php");
                    exit();
                } else {
                    $conn->close();
                    create_message("Error: " . $sql . "<br>" . $conn->error,"danger","warning");
                    header("location:index.php");
                    exit();
                }
              }else{
                  //Kondisi Insert
                  $sql = "INSERT INTO mahasiswa (nama_lengkap, kelas_id, alamat) VALUES ('".$_POST['nama_lengkap']."', ".$_POST['kelas_id'].", '".$_POST['alamat']."')";
                  if ($conn->query($sql) === TRUE) {
                      $conn->close();
                      create_message("Simpan Data Berhasil","success","check");
                      header("location:index.php");
                      exit();
                  } else {
                      $conn->close();
                      create_message("Error: " . $sql . "<br>" . $conn->error,"danger","warning");
                      header("location:index.php");
                      exit();
                  }
              }
            }
        }
?>
