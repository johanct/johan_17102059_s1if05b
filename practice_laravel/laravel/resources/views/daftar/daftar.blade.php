<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link href="../../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<title>Pendaftaran Mahasiswa</title>
</head>
<body>
<div class="container pt-4 bg-white">
<div class="row">
<div class="col-md-8 col-xl-6">
<h1>Daftar</h1>
<hr>
<form action="{{ route('daftar.store') }}" method="POST" enctype="multipart/form-data">
@csrf
</div>

</div>
<div class="form-group">
<label for="username">User Name</label>
<input type="text"
class="form-control @error('username') is-invalid @enderror"
id="username" name="username" value="{{ old('username') }}">
@error('username')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>

<div class="form-group">
<label for="password">Password</label>
<input type="text"
class="form-control @error('password') is-invalid @enderror"
id="password" name="password" value="{{ old('password') }}">
@error('password')
<div class="text-danger">{{ $message }}</div>
@enderror
</div>

<button type="submit" class="btn btn-primary mb-2">Daftar</button>
</form>
</div>
</div>
</div>
</body>
</html>